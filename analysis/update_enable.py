import sys
import os
import argparse
import glob
import json
import shutil


def main():

    # Create output dir
    if not os.path.isdir(output_path):
        os.mkdir(output_path)
        print('Create new directory: '+output_path)

    config_lists = []
    data_lists = []
    firstLoop = True
    for input_file in inputs:
        if not os.path.isfile(input_file):
            print(intput_file+' is not exist.')
            continue
        if firstLoop:
            shutil.copy(input_file, output_path)
            output_name = input_file[(input_file.rfind('/')+1):]
            output_file = output_path+'/'+output_name
            print('output: '+output_file)
            firstLoop = False
        print('input: '+input_file)
        with open(input_file, 'r') as j:
            json_config = json.loads(j.read())
            config_lists.append(json_config)
        input_dir = os.path.dirname(input_file)
        chip_name = json_config[chip_type]['Parameter']['Name']
        scan_file = input_dir+'/'+chip_name+'_Occupancy.json'
        if not os.path.isfile(scan_file):
            print(scan_file+' is not exist.')
            continue
        with open(scan_file, 'r') as j:
            json_data = json.loads(j.read())
            data_lists.append(json_data)
        print('scan file: '+scan_file)
       
    if len(config_lists) == len(data_lists) and len(config_lists) > 0:
        data = data_lists[0]['Data']
        config = config_lists[0][chip_type]['PixelConfig']
        for i in range(len(data_lists)):
            for icol in range(0, len(data)):
                for jrow in range(0, len(data[icol])):
                    hits = data_lists[i]['Data'][icol][jrow]
                    if hits > cut:
                         config[icol]['Enable'][jrow] = 0

        with open(output_file, 'r') as j:
            json_config = json.loads(j.read())
            json_config[chip_type]['PixelConfig'] = config      

        with open(output_file, 'w') as f:
            json.dump(json_config, f, ensure_ascii=False)

    print('Done!')

if __name__ == '__main__':

    # Read command line arguments
    parser = argparse.ArgumentParser(description = 'Make plots of many samples.')
    parser.add_argument('-i', '--inputs', type=str, nargs='+', help='Path to the occupancy file')
    parser.add_argument('-c', '--cut', type=int, help='Cutting value for number of hits')
    parser.add_argument('-o', '--outdir', type=str, help='Output directory')
    parser.add_argument('-t', '--chip_type', type=str, help='choose front end chip type, [RD53A] or [RD53B]', choices=['RD53A', 'RD53B'])
    args = parser.parse_args()
    inputs = args.inputs
    cut = args.cut
    output_path = args.outdir
    chip_type = args.chip_type

    main()

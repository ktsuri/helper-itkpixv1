import sys
import os
import argparse
from array import array
import json
import ROOT

def set_pad(pad, top, bottom, left, right):
    pad.SetTopMargin(top)
    pad.SetBottomMargin(bottom)
    pad.SetLeftMargin(left)
    pad.SetRightMargin(right)
    pad.SetTicks(1,1)
    pad.Draw()
    pad.cd()

def set_occ_palette(h):
    nRGB   = 5
    ncount = 4
    stops  = array('d', [0.00, 0.25, 0.50, 0.75, 1.00] )
    red    = array('d', [1.00, 1.00, 0.20, 0.20, 0.20] )
    green  = array('d', [1.00, 0.00, 0.00, 0.60, 0.60] )
    brue   = array('d', [1.00, 0.20, 0.60, 0.00, 0.00] )
    ROOT.TColor.CreateGradientColorTable(nRGB, stops, red, green, brue, ncount);
    ROOT.gStyle.SetNumberContours(ncount)
    h.GetZaxis().SetRangeUser(0, ncount)
    h.GetZaxis().SetNdivisions(ncount, 1, 1, 0)
    h.GetZaxis().ChangeLabel(1, 90., -1., -1, -1, -1, "    Mask")
    h.GetZaxis().ChangeLabel(2, 90., -1., -1, -1, -1, "     0 hit")
    h.GetZaxis().ChangeLabel(3, 90., -1., -1, -1, -1, "occ<1%")
    h.GetZaxis().ChangeLabel(4, 90., -1., -1, -1, -1, "  over 1%")
    h.GetZaxis().ChangeLabel(5, 90., -1., -1, -1, -1, " ")

def dress_2D(h):
    ROOT.gStyle.SetOptStat(ROOT.kFALSE)
    # x axis
    h.GetXaxis().SetTitleOffset(1.)
    h.GetXaxis().SetTitleSize(0.05)
    h.GetXaxis().SetLabelOffset(0.01)
    h.GetXaxis().SetLabelSize(0.05)
    # y axis
    h.GetYaxis().SetTitleOffset(1.3)
    h.GetYaxis().SetTitleSize(0.05)
    h.GetYaxis().SetLabelOffset(0.01)
    h.GetYaxis().SetLabelSize(0.05)
    # z axis
    h.GetZaxis().SetTitleOffset(1.2)
    h.GetZaxis().SetTitleSize(0.05)
    h.GetZaxis().SetLabelOffset(0.03)
    h.GetZaxis().SetLabelSize(0.05)

def dress_1D(h):
    ROOT.gStyle.SetOptStat(ROOT.kFALSE)
    # x axis
    h.GetXaxis().SetTitleOffset(1.)
    h.GetXaxis().SetTitleSize(0.05)
    h.GetXaxis().SetLabelOffset(0.01)
    h.GetXaxis().SetLabelSize(0.05)
    # y axis
    h.GetYaxis().SetTitleOffset(1.6)
    h.GetYaxis().SetTitleSize(0.05)
    h.GetYaxis().SetLabelOffset(0.01)
    h.GetYaxis().SetLabelSize(0.05)
    # line
    h.SetLineWidth(2)

def get_max(data):
    maximum = 0
    for i in range(len(data)):
        imax = max(data[i])
        if maximum < imax:
            maximum = imax
    return maximum

def main():

    # load json file
    if not os.path.isfile(input_file):
        print(input_file+' is not exist.')
        sys.exit()
    with open(input_file, 'r') as j: 
        json_data = json.loads(j.read())
    if not os.path.isfile(config_file):
        print(config_file+' is not exist.')
        sys.exit()
    with open(config_file, 'r') as j:
        json_config = json.loads(j.read())

    # create output dir
    if not os.path.isdir(output_path):
        os.mkdir(output_path)
        print('Create new directory: '+output_path)

    filename = os.path.basename(input_file)
    file_name = filename[:filename.rfind('.')]
    output_png = output_path+'/'+file_name+'_AfterClassify.png'

    # read config/data file
    data = json_data['Data']
    pixel_config = json_config[chip_type]['PixelConfig']

    # set histogram and canvas
    maximum = get_max(data)
    ncol = len(pixel_config)
    nrow = len(pixel_config[0]['Enable'])
    canv = ROOT.TCanvas('canv', '', 0, 0, 1000, 900)
    pad = ROOT.TPad("", "", 0, 0.0, 1, 1)
    hmap = ROOT.TH2D('',';Col;Row;',ncol,0.5,ncol+0.5,nrow,0.5,nrow+0.5)
    hist = ROOT.TH1D('',file_name+ ';number of hits;number of pixels', int(maximum+50), 0, maximum+50)

    # fill the histogram
    for icol in range(ncol):
        for jrow in range(nrow):
            hits = data[icol][jrow]
            enable = pixel_config[icol]['Enable'][jrow]
            if not enable == 0:
                hist.Fill(hits)

    # fit gaus
    avg0, std0 = hist.GetMean(), hist.GetStdDev()
    gaus = ROOT.TF1("gausfit", "gaus", avg0-std0, avg0+std0)
    hist.Fit(gaus, "R") # fit inside a given range
    avg, std = gaus.GetParameter(1), gaus.GetParameter(2)

    # fill the classify map
    occ = avg * 0.01
    print(avg)
    print(occ)
    for icol in range(ncol):
        for jrow in range(nrow):
            hits = data[icol][jrow]
            enable = pixel_config[icol]['Enable'][jrow]
            if enable == 0:
                hmap.SetBinContent(icol+1, jrow+1, hits)
            else:
                if hits == 0:
                    hmap.SetBinContent(icol+1, jrow+1, 1) # 0hit
                elif hits < occ:
                    print(hits)
                    hmap.SetBinContent(icol+1, jrow+1, 2) # occ < 1%
                else:
                    hmap.SetBinContent(icol+1, jrow+1, 3) # 

    # draw histgram
    set_pad(pad, 0.10, 0.13, 0.16, 0.19)
    dress_2D(hmap)
    set_occ_palette(hmap)
    hmap.Draw("colz")
    canv.Print(output_path+'/'+file_name+'_AfterClassify.png')
    pad.Close()
    canv.Update()


    set_pad(pad, 0.10, 0.13, 0.18, 0.09)
    dress_1D(hist)
    hist.Draw()
    canv.Print(output_path+'/'+file_name+'Dist.png')
    hist.GetXaxis().SetRangeUser(0, 100)
    canv.Print(output_path+'/'+file_name+'Dist_zoom100.png')
    pad.SetLogy(1)
    canv.Print(output_path+'/'+file_name+'Dist_zoom100_log.png')
    pad.Close()
    canv.Update()

    print('Done!')


if __name__ == '__main__':

    # Set to batch mode -> do not display graphics
    ROOT.gROOT.SetBatch(True)

    # Read command line arguments
    parser = argparse.ArgumentParser(description = 'Make plots of many samples.')
    parser.add_argument('-i', '--inputfile', type=str, help='Input the path of chip config file')
    parser.add_argument('-c', '--config', type=str, help='Input the path to configuration file of each modules')
    parser.add_argument('-o', '--outdir', type=str, help='Output directory')
    parser.add_argument('-t', '--chip_type', type=str, help='choose front end chip type, [RD53A] or [RD53B]', choices=['RD53A', 'RD53B'])
    args = parser.parse_args()
    input_file = args.inputfile
    config_file = args.config
    output_path = args.outdir
    chip_type = args.chip_type

    main()

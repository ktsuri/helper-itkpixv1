import sys
import os
import argparse
from array import array
import json
import glob
import ROOT

def findFrontEnd(input_dir):
    if not input_dir.rfind('std')==-1:
        return [0,400]
    elif not input_dir.rfind('lindiff')==-1:
        return [128,400]
    elif not input_dir.rfind('syn')==-1:
        return [0,128]
    else:
        return [0,400]

def set_pad(pad, top, bottom, left, right):
    pad.SetTopMargin(top)
    pad.SetBottomMargin(bottom)
    pad.SetLeftMargin(left)
    pad.SetRightMargin(right)
    pad.SetTicks(1,1)
    pad.Draw()
    pad.cd()

def dress_1D(h):
    ROOT.gStyle.SetOptStat(ROOT.kFALSE)
    # x axis
    h.GetXaxis().SetTitleOffset(1.)
    h.GetXaxis().SetTitleSize(0.05)
    h.GetXaxis().SetLabelOffset(0.)
    h.GetXaxis().SetLabelSize(0.035)
    # y axis
    h.GetYaxis().SetTitleOffset(1.3)
    h.GetYaxis().SetTitleSize(0.05)
    h.GetYaxis().SetLabelOffset(0.01)
    h.GetYaxis().SetLabelSize(0.05)

def set_xlabel(h, category, names, n):
    h.GetXaxis().SetNdivisions(n, 1, 1, 0)
    nn = 2
    for i in range(len(category)):
        for j in range(len(names)):
            nn = nn + j
            h.GetXaxis().ChangeLabel(nn, 300., -1., 1, -1, -1, names[j]+', '+category[i])
        nn = nn + 2
        h.GetXaxis().ChangeLabel(1+(len(names)+1)*i, 0., -1., 1, -1, -1, ' ')
    h.GetXaxis().ChangeLabel(1+(len(names)+1)*len(category), 0., -1., 1, -1, -1, ' ')
    h.GetXaxis().ChangeLabel(1+(len(names)+1)*len(category)+1, 0., -1., 1, -1, -1, ' ')

def main():

    # validate input length
    if len(input_dirs) < 1:
        print('Input arguments are too short.')
        sys.exit()

    # create output dir
    if not os.path.isdir(output_path):
        os.mkdir(output_path)
        print('Create new directory: '+output_path)

    opt = 0
    width = 1
    category = ['Mask', '0hit', '< 10 hits', 'over 10hits']
    colors = [[ROOT.kRed+1, ROOT.kRed+2, ROOT.kRed-3, ROOT.kRed-4],[ROOT.kBlue+1, ROOT.kBlue+2, ROOT.kBlue-3, ROOT.kBlue-4], [ROOT.kGreen+1, ROOT.kGreen+2, ROOT.kGreen-3, ROOT.kGreen-4]]
    canv = ROOT.TCanvas('canv', '', 0, 0, 1000, 600)
    pad = ROOT.TPad("", "", 0, 0.0, 1, 1)
    set_pad(pad, 0.08, 0.30, 0.14, 0.05) 
    leg  = ROOT.TLegend(0.18,0.60,0.30,0.91)
    hists = [ [] for i in range(len(input_dirs)) ]
    output_title = ''
    title = ''
    for iname in range(len(names)-1):
        output_title = output_title + names[iname] + '_vs_'
        title = title + names[iname] + ' vs '
    output_title = output_title+str(names[-1])
    title = title+str(names[-1])

    for input_dir in input_dirs:
        k = input_dirs.index(input_dir)
        name = names[k]
        print('----------'+name+'----------')

        if not os.path.isdir(input_dir):
            print(input_dir+' is not exist.')
            sys.exit()
        col = findFrontEnd(input_dir)
        config_path = input_dir+'/*.json.before'
        configs = glob.glob(config_path)
        if k==0:
            num = len(input_dirs) * len(configs)
            if not num%2 ==0:
                num=num-1
 
        hist_list = []
        for config in configs:
            l = configs.index(config)

            # load config file
            with open(config, 'r') as j:
                json_config = json.loads(j.read())
            print(config)
            chip_name = json_config[chip_type]['Parameter']['Name']
            pixel_config = json_config[chip_type]['PixelConfig']

            # load scan file
            scan_file = input_dir+'/'+chip_name+'_Occupancy.json'
            if not os.path.isfile(scan_file):
                print(scan_file+' is not exist.')
                sys.exit()
            with open(scan_file, 'r') as j:
                json_scan = json.loads(j.read())
            print(scan_file)

            # set histogram
            xhigh = num*len(category)+num*(len(category)-2)+num/2
            hists[k].append(ROOT.TH1D('', title + ';;number of pixels', int(xhigh), 0, xhigh))

            # read data
            data = json_scan['Data']
            #nclass = {category[0]:0,category[1]:0,category[2]:0,category[3]:0}
            for icol in range(col[0],col[1]):
                column = pixel_config[icol]
                for jrow in range(0,len(data[icol])):
                    hits = data[icol][jrow]
                    enable = column['Enable'][jrow]
                    if enable == 0:
                        #nclass[category[0]]=nclass[category[0]]+1
                        hists[k][l].Fill(opt+1.5*num*0+num/2)
                    else:
                        if hits == 0:
                            #nclass[category[1]]=nclass[category[1]]+1
                            hists[k][l].Fill(opt+1.5*num*1+num/2)
                        elif hits < 10:
                            #nclass[category[2]]=nclass[category[2]]+1
                            hists[k][l].Fill(opt+1.5*num*2+num/2)
                        else:
                            #nclass[category[3]]=nclass[category[3]]+1
                            hists[k][l].Fill(opt+1.5*num*3+num/2)

            leg_title = name+' '+chip_name
            leg.AddEntry(hists[k][l], leg_title, 'f')
            hists[k][l].SetLineColor(colors[k][l])
            hists[k][l].SetFillColor(colors[k][l])
            dress_1D(hists[k][l])
            if k==0 and l==0:
                set_xlabel(hists[k][l],category, names, int(xhigh/(num/2)))
                hists[k][l].Draw()
            else:
                hists[k][l].Draw('same')

            opt = opt+width

    leg.SetLineColor(ROOT.kWhite)
    leg.SetTextSize(0.03)
    leg.SetFillStyle(0)
    leg.Draw()
    canv.Print(output_path+'/sourcescan_'+output_title+'.png') 
    pad.SetLogy(1)
    canv.Print(output_path+'/sourcescan_'+output_title+'_log.png') 

    print('Done!')

if __name__ == '__main__':

    # Set to batch mode -> do not display graphics
    ROOT.gROOT.SetBatch(True)

    # Read command line arguments
    parser = argparse.ArgumentParser(description = 'Make plots of many samples.')
    parser.add_argument('-i', '--inputs', type=str, nargs='+', help='Path to the occupancy map file of each modules')
    parser.add_argument('-n', '--names', type=str, nargs='+', help='name')
    parser.add_argument('-o', '--outdir', type=str, help='Output directory')
    parser.add_argument('-t', '--chip_type', type=str, help='choose front end chip type, [RD53A] or [RD53B]', choices=['RD53A', 'RD53B'])
    args = parser.parse_args()
    input_dirs = args.inputs
    names = args.names
    output_path = args.outdir
    chip_type = args.chip_type

    main()

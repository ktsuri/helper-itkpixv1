import sys
import os
import argparse
import glob
import ROOT
import json
import math
import numpy as np
from array import array

def set_pad(pad, top, bottom, left, right):
    pad.SetTopMargin(top)
    pad.SetBottomMargin(bottom)
    pad.SetLeftMargin(left)
    pad.SetRightMargin(right)
    pad.SetTicks(1,1)
    pad.Draw()
    pad.cd()

def dress_1D(h):
    ROOT.gStyle.SetOptStat(ROOT.kFALSE)
    # x axis
    h.GetXaxis().SetTitleOffset(1.2)
    h.GetXaxis().SetTitleSize(0.05)
    h.GetXaxis().SetLabelOffset(0.)
    h.GetXaxis().SetLabelSize(0.05)
    # y axis
    h.GetYaxis().SetTitleOffset(1.3)
    h.GetYaxis().SetTitleSize(0.05)
    h.GetYaxis().SetLabelOffset(0.01)
    h.GetYaxis().SetLabelSize(0.05)

def main():

    # validate input length
    if len(inputs) < 2:
        print('Input arguments are too short.')
        sys.exit()
    if len(inputs) != len(names):
        print('Name and Input arguments are not same length.')
        sys.exit()

    # create output dir
    if not os.path.isdir(output_path):
        os.mkdir(output_path)
        print('Create new directory: '+output_path)

    colors = [ROOT.kRed, ROOT.kBlue, ROOT.kGreen]
    hists = []
    name_lists = []
    x_range = [0,0]
    for input_file in inputs:
        if not os.path.isfile(input_file):
            print(intput_file+' is not exist.')
            continue
        with open(input_file, 'r') as j:
            json_data = json.loads(j.read())

        # set histogram
        ztitle = json_data['z']['AxisTitle']
        h = ROOT.TH1D('',';'+ztitle+';number of pixels',100000,0,100000)

        data = json_data['Data']
        for icol in range(0,len(data)):
            for jrow in range(0,len(data[icol])):
                hits = data[icol][jrow]
                h.Fill(hits)
        hists.append(h)
        name = names[inputs.index(input_file)]
        print(name)
        name_lists.append(name)
        xmax = h.FindLastBinAbove()
        xmin = h.FindFirstBinAbove()
        if x_range[0] < xmax:
            x_range[0] = xmax
        if xmin < x_range[1]:
            x_range[1] = xmin

    # set canvas and pad
    canv = ROOT.TCanvas("canv", "", 0, 0, 1000, 900)
    pad = ROOT.TPad("", "", 0, 0.0, 1, 1)
    leg  = ROOT.TLegend(0.18,0.70,0.30,0.91)

    firstLoop = True
    for i in range(len(hists)):
        hists[i].SetLineColor(colors[i])
        leg.AddEntry(hists[i], name_lists[i], 'f')
        if firstLoop:
            set_pad(pad, 0.08, 0.12, 0.14, 0.05) 
            dress_1D(hists[i])
            hists[i].Draw()
            hists[i].GetXaxis().SetRangeUser(xmin, xmax)
            firstLoop = False
        else:
            hists[i].Draw('same')

    leg.SetLineColor(ROOT.kWhite)
    leg.SetTextSize(0.03)
    leg.SetFillStyle(0)
    leg.Draw()
    canv.Print(output_path+'/plot2dist_test.png')
    pad.SetLogy(1)
    canv.Print(output_path+'/plot2dist_test_log.png')

    print("Done")

if __name__ == "__main__":

    # Set to batch mode -> do not display graphics
    #ROOT.gROOT.SetBatch(True)

    # Read command line arguments
    parser = argparse.ArgumentParser(description = 'Make plots of many samples.')
    parser.add_argument('-i', '--inputs', type=str, nargs='+', help='Path to the occupancy file')
    parser.add_argument('-n', '--names', type=str, nargs='+', help='name')
    parser.add_argument('-o', '--outdir', type=str, help='Output directory')
    args = parser.parse_args()
    inputs = args.inputs
    names = args.names
    output_path = args.outdir

    main()

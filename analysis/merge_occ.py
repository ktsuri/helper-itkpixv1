import sys
import os
import argparse
import json
import shutil

def main():

    # validate input length
    if len(inputs) < 2:
        print('Input arguments are too short.')
        sys.exit()

    # create output dir
    if not os.path.isdir(output_path):
        os.mkdir(output_path)
        print('Create new directory: '+output_path)

    data_lists = []
    firstLoop = True
    for input_file in inputs:
        if not os.path.isfile(input_file):
            print(intput_file+' is not exist.')
            continue
        if firstLoop:
            shutil.copy(input_file, output_path)
            output_name = input_file[(input_file.rfind('/')+1):]
            output_file = output_path+'/'+output_name
            print('output: '+output_file)
            firstLoop = False
        print('input: '+input_file)
        with open(input_file, 'r') as j:
            json_data = json.loads(j.read())
            data_lists.append(json_data) 

    if len(data_lists) > 1:
        data = data_lists[0]['Data']
        for i in range(1, len(data_lists)):
            for icol in range(0, len(data)):
                for jrow in range(0, len(data[icol])):
                    data[icol][jrow] += data_lists[i]['Data'][icol][jrow]
                    
        with open(output_file, 'r') as j:
            json_data = json.loads(j.read())
            json_data['Data'] = data

        with open(output_file, 'w', encoding='utf-8') as f:
            json.dump(json_data, f, ensure_ascii=False)

    print('Done!')

if __name__ == '__main__':

    # Read command line arguments
    parser = argparse.ArgumentParser(description = 'Make plots of many samples.')
    parser.add_argument('-i', '--inputs', type=str, nargs='+', help='Path to the occupancy map file of each modules')
    parser.add_argument('-o', '--outdir', type=str, help='Output directory')
    args = parser.parse_args()
    inputs = args.inputs
    output_path = args.outdir

    main()

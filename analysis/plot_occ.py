import sys
import os
import argparse
from array import array
import json
import ROOT

def set_pad(pad, top, bottom, left, right):
    pad.SetTopMargin(top)
    pad.SetBottomMargin(bottom)
    pad.SetLeftMargin(left)
    pad.SetRightMargin(right)
    pad.SetTicks(1,1)
    pad.Draw()
    pad.cd()

def set_occ_palette():
    nRGBs  = 5
    ncount = 255
    stops  = array('d', [0.00, 0.34, 0.61, 0.84, 1.00] )
    red    = array('d', [0.00, 0.00, 0.87, 1.00, 0.51] )
    green  = array('d', [0.00, 0.81, 1.00, 0.20, 0.00] )
    blue   = array('d', [0.51, 1.00, 0.12, 0.00, 0.00] )
    ROOT.TColor.CreateGradientColorTable(nRGBs, stops, red, green, blue, ncount)
    ROOT.gStyle.SetNumberContours(ncount)

def dress_2D(h):
    ROOT.gStyle.SetOptStat(ROOT.kFALSE)
    # x axis
    h.GetXaxis().SetTitleOffset(1.)
    h.GetXaxis().SetTitleSize(0.05)
    h.GetXaxis().SetLabelOffset(0.01)
    h.GetXaxis().SetLabelSize(0.05)
    # y axis
    h.GetYaxis().SetTitleOffset(1.3)
    h.GetYaxis().SetTitleSize(0.05)
    h.GetYaxis().SetLabelOffset(0.01)
    h.GetYaxis().SetLabelSize(0.05)
    # z axis
    h.GetZaxis().SetTitleOffset(1.2)
    h.GetZaxis().SetTitleSize(0.05)
    h.GetZaxis().SetLabelOffset(0.01)
    h.GetZaxis().SetLabelSize(0.05)

def main():

    # load input json file
    if not os.path.isfile(input_file):
        print(input_file+' is not exist.')
        sys.exit()
    with open(input_file, 'r') as j: 
        json_data = json.loads(j.read())

    # create output dir
    if not os.path.isdir(output_path):
        os.mkdir(output_path)
        print('Create new directory: '+output_path)

    filename = os.path.basename(input_file)
    file_name = filename[:filename.rfind('.')]
    output_png = output_path+'/'+file_name+'.png'

    # set histogram and canvas
    xbins, xlow, xhigh = json_data['x']['Bins'], json_data['x']['Low'], json_data['x']['High']
    ybins, ylow, yhigh = json_data['y']['Bins'], json_data['y']['Low'], json_data['y']['High']
    xtitle, ytitle, ztitle = json_data['x']['AxisTitle'], json_data['y']['AxisTitle'], json_data['z']['AxisTitle']
    canv = ROOT.TCanvas('canv', '', 0, 0, 1000, 900)
    pad = ROOT.TPad("", "", 0, 0.0, 1, 1)
    h = ROOT.TH2D('',file_name+';'+xtitle+';'+ytitle+';'+ztitle,xbins,xlow,xhigh,ybins,ylow,yhigh)

    # fill
    data = json_data['Data']
    for icol in range(0,len(data)):
        for jrow in range(0,len(data[icol])):
            hits = data[icol][jrow]
            h.SetBinContent(icol+1, jrow+1, hits)

    # draw histgram
    set_pad(pad, 0.10, 0.13, 0.16, 0.19)
    dress_2D(h)
    set_occ_palette()
    h.Draw("colz")
    canv.Print(output_png)

    print('Done!')

if __name__ == '__main__':

    # Set to batch mode -> do not display graphics
    ROOT.gROOT.SetBatch(True)

    # Read command line arguments
    parser = argparse.ArgumentParser(description = 'Make plots of many samples.')
    parser.add_argument('-i', '--input_file', type=str, help='Input the path of occupancy map file')
    parser.add_argument('-o', '--outdir', type=str, help='Output directory')
    args = parser.parse_args()
    input_file = args.input_file
    output_path = args.outdir

    main()

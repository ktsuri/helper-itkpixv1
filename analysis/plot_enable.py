import sys
import os
import argparse
from array import array
import json
import ROOT

def set_pad(pad, top, bottom, left, right):
    pad.SetTopMargin(top)
    pad.SetBottomMargin(bottom)
    pad.SetLeftMargin(left)
    pad.SetRightMargin(right)
    pad.SetTicks(1,1)
    pad.Draw()
    pad.cd()

def set_enable_palette(h):
    nRGB   = 4
    ncount = 2
    stops  = array('d', [0.00, 0.49, 0.50, 1.00] )
    red    = array('d', [1.00, 1.00, 0.20, 0.20] )
    green  = array('d', [1.00, 1.00, 0.60, 0.60] )
    brue   = array('d', [1.00, 1.00, 0.00, 0.00] )
    ROOT.TColor.CreateGradientColorTable(nRGB, stops, red, green, brue, ncount)
    ROOT.gStyle.SetNumberContours(ncount)
    h.GetZaxis().SetLabelOffset(0.03)
    h.GetZaxis().SetNdivisions(2, 1, 1, 0)
    h.GetZaxis().ChangeLabel(1, 90., -1., -1, -1, -1, "             Disable")
    h.GetZaxis().ChangeLabel(2, 90., -1., -1, -1, -1, "             Enable")
    h.GetZaxis().ChangeLabel(3, 90., -1., -1, -1, -1, " ")

def dress_2D(h):
    ROOT.gStyle.SetOptStat(ROOT.kFALSE)
    # x axis
    h.GetXaxis().SetTitleOffset(1.)
    h.GetXaxis().SetTitleSize(0.05)
    h.GetXaxis().SetLabelOffset(0.01)
    h.GetXaxis().SetLabelSize(0.05)
    # y axis
    h.GetYaxis().SetTitleOffset(1.3)
    h.GetYaxis().SetTitleSize(0.05)
    h.GetYaxis().SetLabelOffset(0.01)
    h.GetYaxis().SetLabelSize(0.05)
    # z axis
    h.GetZaxis().SetTitleOffset(1.2)
    h.GetZaxis().SetTitleSize(0.05)
    h.GetZaxis().SetLabelOffset(0.01)
    h.GetZaxis().SetLabelSize(0.05)

def main():

    # load input json file
    if not os.path.isfile(input_file):
        print(input_file+' is not exist.')
        sys.exit()
    with open(input_file, 'r') as j: 
        json_config = json.loads(j.read())

    # create output dir
    if not os.path.isdir(output_path):
        os.mkdir(output_path)
        print('Create new directory: '+output_path)

    filename = os.path.basename(input_file)
    file_name = filename[:filename.rfind('.')]
    output_png = output_path+'/'+file_name+'_EnableMap.png'

    # set histogram and canvas
    pixel_config = json_config[chip_type]['PixelConfig']
    ncol = len(pixel_config)
    nrow = len(pixel_config[0]['Enable'])
    canv = ROOT.TCanvas('canv', '', 0, 0, 1000, 900)
    pad = ROOT.TPad("", "", 0, 0.0, 1, 1)
    h = ROOT.TH2D('',file_name+' Enable Map;Col;Row;',ncol,0.5,ncol+0.5,nrow,0.5,nrow+0.5)

    # fill
    for icol in range(0,ncol):
        col = pixel_config[icol]
        for jrow in range(0,nrow):
            enable = col['Enable'][jrow]
            if enable == 1:
                h.Fill(icol+1, jrow+1)

    # draw histgram
    set_pad(pad, 0.10, 0.13, 0.16, 0.19)
    dress_2D(h)
    set_enable_palette(h)
    h.Draw("colz")
    canv.Print(output_png)

    print('Done!')

if __name__ == '__main__':

    # Set to batch mode -> do not display graphics
    ROOT.gROOT.SetBatch(True)

    # Read command line arguments
    parser = argparse.ArgumentParser(description = 'Make plots of many samples.')
    parser.add_argument('-i', '--input_file', type=str, help='Input the path of chip config file')
    parser.add_argument('-o', '--outdir', type=str, help='Output directory')
    parser.add_argument('-t', '--chip_type', type=str, help='choose front end chip type, [RD53A] or [RD53B]', choices=['RD53A', 'RD53B'])
    args = parser.parse_args()
    input_file = args.input_file
    output_path = args.outdir
    chip_type = args.chip_type

    main()

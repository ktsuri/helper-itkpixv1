import sys
import os
import argparse
from array import array
import json
import ROOT

def main():

    # load input json file
    if not os.path.isfile(input_file):
        print(input_file+' is not exist.')
        sys.exit()
    with open(input_file, 'r') as j: 
        json_config = json.loads(j.read())

    startTime = json_config["startTime"] 
    finishTime = json_config["finishTime"] 
    time = finishTime - startTime
    time_min = round(time/60)
    print('scan time: '+str(time)+'[s], '+str(time_min)+'[min]')

if __name__ == '__main__':

    # Read command line arguments
    parser = argparse.ArgumentParser(description = 'Make plots of many samples.')
    parser.add_argument('-i', '--input_file', type=str, help='Input the path of scan log file(scanLog.json)')
    args = parser.parse_args()
    input_file = args.input_file

    main()

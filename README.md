# Helper scripts for ITkPixV1 modules

This repository contains scripts for copying chip config file and manually create new masks from various scans (`analysis/update_mask.py`).

## Quick Install Guide:

* Clone from git
  * `git clone https://gitlab.cern.ch/ktsuri/helper-itkpixv1.git helper-itkpixv1`

## Requirements

* Python3.6

## How to use


* Plot for the occupancy map
  * `python3 analysis/plot_occ.py -i <Path of Occupancy.json or OccupancyMap.json> -o <Output Directory>`
* Plot for the enable map
  * `python3 analysis/plot_enable.py -i <Path of the chip config file> -o <Output Directory> -t <Select chip type [RD53A or RD53B]>`

